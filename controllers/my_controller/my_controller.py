from controller import Robot, DistanceSensor, Motor
import numpy as np

# time in [ms] of a simulation step
TIME_STEP = 64
MAX_SPEED = 6.28

# create the Robot instance.
robot = Robot()

# initialize proximity sensors
ps = []
psNames = ['ps0', 'ps1', 'ps2', 'ps3', 'ps4', 'ps5', 'ps6', 'ps7']

# initialize light sensors
ls = []
lsNames = ['ls0', 'ls1', 'ls2', 'ls3', 'ls4', 'ls5', 'ls6', 'ls7']

for i in range(8):
    ps.append(robot.getDevice(psNames[i]))
    ls.append(robot.getDevice(lsNames[i]))
    ps[i].enable(TIME_STEP)
    ls[i].enable(TIME_STEP)

gps = robot.getDevice('gps')
gps.enable(TIME_STEP)


leftEncoder = robot.getDevice('left wheel sensor')
leftEncoder.enable(TIME_STEP)
rightEncoder = robot.getDevice('right wheel sensor')
rightEncoder.enable(TIME_STEP)

leftMotor = robot.getDevice('left wheel motor')
rightMotor = robot.getDevice('right wheel motor')
leftMotor.setPosition(float('inf'))
rightMotor.setPosition(float('inf'))
leftMotor.setVelocity(0.0)
rightMotor.setVelocity(0.0)

# feedback loop: step simulation until receiving an exit event
while robot.step(TIME_STEP) != -1:
    # initialize motor speeds at 50% of MAX_SPEED.
    leftSpeed  = 0.5 * MAX_SPEED
    rightSpeed = 0.5 * MAX_SPEED

    # read encoders
    rightEncoderValue = rightEncoder.getValue()
    leftEncoderValue = leftEncoder.getValue()

    # read position
    x = gps.getValues()[0]
    y = gps.getValues()[1]
    print("x: " + str(x))
    print("y: " + str(y))


    # read sensors outputs
    psValues = []
    lsValues = []
    for i in range(8):
        psValues.append(ps[i].getValue())
        lsValues.append(ls[i].getValue())
    
    # detect light
    left_light = np.mean([lsValues[5], lsValues[6], lsValues[7]])
    right_light = np.mean([lsValues[0], lsValues[1], lsValues[2]])


    # detect obstacles
    right_obstacle = psValues[0] > 80.0 or psValues[1] > 80.0 or psValues[2] > 80.0
    left_obstacle = psValues[5] > 80.0 or psValues[6] > 80.0 or psValues[7] > 80.0

    # modify speeds according to obstacles
    if left_obstacle:
        # turn right
        leftSpeed  = 0.5 * MAX_SPEED
        rightSpeed = -0.5 * MAX_SPEED
    elif right_obstacle:
        # turn left
        leftSpeed  = -0.5 * MAX_SPEED
        rightSpeed = 0.5 * MAX_SPEED

    # write actuators inputs
    leftMotor.setVelocity(leftSpeed)
    rightMotor.setVelocity(rightSpeed)